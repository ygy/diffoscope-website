---
layout: post
title: diffoscope 28 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `28`. This version includes the following changes:

```
* Improve code quality by storing comments for Difference as lists.
* Fix label for content of gzip, bzip2, and xz files.
* Handle when only the encoding is different when comparing text files.
  (Closes: #794347)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
