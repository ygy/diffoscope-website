---
layout: post
title: diffoscope 76 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `76`. This version includes the following changes:

```
[ Chris Lamb ]
* Extract archive members using an auto-incrementing integer, avoiding the
  need to sanitise filenames and avoiding writes to arbitrary locations.
  (Closes: #854723 - CVE-2017-0359)

[ Ximin Luo ]
* Simplify call to subprocess.Popen
```

You find out more by [visiting the project homepage](https://diffoscope.org).
