---
layout: post
title: diffoscope 13 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `13`. This version includes the following changes:

```
[ Helmut Grohne ]
* Fix several imports for RequiredToolNotFound.

[ Jérémy Bobbio ]
* Remove dead code from HTML presenter.
* Fix tab handling in HTML presenter.
* Swallow stderr when running showttf. (Closes: #781374)
* Swallow stderr for readelf and objdump.
* Use pipes instead of temp files to feed diff.
* Stop processing diff output after too many lines.
* Use LINESIZE constant directly in HTML presenter.
* Better handle non-printable characters in HTML presenter.
* Cut long lines in HTML presenter.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
