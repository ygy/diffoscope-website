---
layout: post
title: diffoscope 54 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `54`. This version includes the following changes:

```
* Fix syntax in RequiredToolNotFound.PROVIDERS.  This caused --list-tools to
  miss entries, and by it missing Recommends in the Debian package.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
