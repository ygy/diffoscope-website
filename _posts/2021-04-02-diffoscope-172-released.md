---
layout: post
title: diffoscope 172 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `172`. This version includes the following changes:

```
* If zipinfo(1) shows a difference but we cannot uncover a difference within
  the underlying .zip or .apk file, add a comment and show the binary
  comparison. (Closes: reproducible-builds/diffoscope#246)
* Make "error extracting X, falling back to binary comparison E" error
  message nicer.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
