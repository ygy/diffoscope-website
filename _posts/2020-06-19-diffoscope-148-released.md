---
layout: post
title: diffoscope 148 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `148`. This version includes the following changes:

```
[ Daniel Fullmer ]
* Fix a regression in the CBFS comparator due to changes in our_check_output.

[ Chris Lamb ]
* Add a remark in the deb822 handling re. potential security issue in the
  .changes, .dsc, .buildinfo comparator.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
