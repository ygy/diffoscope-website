---
layout: post
title: diffoscope 189 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `189`. This version includes the following changes:

```
[ Chris Lamb ]
* Try some alternative suffixes (eg. ".py") to support distributions that
  strip or retain them. (Closes: reproducible-builds/diffoscope#283)
* Skip Python bytecode testing where we do not have an expected diff.
  (Closes: reproducible-builds/diffoscope#284)
* Refactor the find_executable utility into an explicit method.
* Split out a custom call to assert_diff to support a .startswith equivalent.
* Use skipif instead of manual conditionals in some tests.

[ Vagrant Cascadian ]
* Add an external tool reference for Guix to support ppudump and dumppdf.

[ Sergei Trofimovich ]
* Update uImage test output for file(1) version 5.41.

[ Jelle van der Waa ]
* Add Arch Linux as CI test target.
* Add external tools on Arch Linux for ffmpeg, openssl and ocalobjinfo.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
