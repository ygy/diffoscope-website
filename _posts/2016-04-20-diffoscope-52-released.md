---
layout: post
title: diffoscope 52 released
author: Reiner Herrmann <reiner@reiner-h.de>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `52`. This version includes the following changes:

```
[ Mattia Rizzolo ]
* debian/gbp.conf: add a conf to have gbp tag the releases how we like them.

[ Holger Levsen ]
* Drop transitional debbindiff package.
* debian/control: Drop XS-Prefix from Testsuite: field, thanks lintian.
* Mention --max-report-size only once in the manpage. (Closes: #819956)

[ Satyam Zode ]
* Fix typo in documentation.

[ Reiner Herrmann ]
* Bump Standards-Version to 3.9.8, no changes required.
* Let objdump demangle symbols for better readability.
* Install bin/diffoscope instead of auto-generated script. (Closes: #821777)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
