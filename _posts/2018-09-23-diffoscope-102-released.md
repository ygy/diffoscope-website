---
layout: post
title: diffoscope 102 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `102`. This version includes the following changes:

```
[ Chris Lamb ]
* Fix tests under colord >= 1.4.3.  Closes: #908900

[ Xavier Briand ]
* Add an "Add a comparator" section in CONTRIBUTING.  MR: !9

[ Mattia Rizzolo ]
* debian: Use the new debhelper-compat(=11) build dep and drop d/compat.

[ Marek Marczykowski-Górecki ]
* comparators/json: Try fuzzy matching for non-text files too.
  This avoids loading very large file just to discover they aren't JSON.
  Closes: #909122
```

You find out more by [visiting the project homepage](https://diffoscope.org).
