---
layout: post
title: diffoscope 33 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `33`. This version includes the following changes:

```
* Fix path to diffoscope used to generate Recommends. (Closes: #797978)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
