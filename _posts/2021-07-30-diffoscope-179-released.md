---
layout: post
title: diffoscope 179 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `179`. This version includes the following changes:

```
* Ensure that various LLVM tools are installed, even when testing whether
  a MacOS binary has zero differences when compared to itself.
  (Closes: reproducible-builds/diffoscope#270)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
