---
layout: post
title: diffoscope 97 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `97`. This version includes the following changes:

```
* Create all temporary directories within a top-level dir. (Closes: #902627)
* tests/conftest.py: Fix compatibility with pytest 3.6.2-1, currently in
  Debian experimental.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
