---
layout: post
title: diffoscope 181 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `181`. This version includes the following changes:

```
[ Chris Lamb ]

* New features and bug fixes:
  - Don't require apksigner in order to compare .apk files using apktool.
  - Add a special-case to squshfs image extraction to not fail if we aren't
    root/superuser. (Closes: #991059)
  - Reduce the maximum line length to avoid O(n^2) Wagner-Fischer algorithm,
    which meant that diff generation took an inordinate amount of time.
    (Closes: reproducible-builds/diffoscope#272)
  - Include profiling information in --debug output if --profile is not set.
  - Don't print an orphan newline when the Black source code formatter
    self-test passes.

* Tests:
  - Update test to check specific contents of squashfs listing, otherwise it
    fails depending on the test systems uid-to-username mapping in passwd(5).
  - Assign "seen" and "expected" values to local variables to improve
    contextual information in/around failed tests.

* Misc changes:
  - Print the size of generated HTML, text (etc.) reports.
  - Profile calls to specialize and diffoscope.diff.linediff.
  - Update various copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
