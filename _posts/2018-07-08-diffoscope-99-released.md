---
layout: post
title: diffoscope 99 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `99`. This version includes the following changes:

```
[ Xavier Briand ]
* Add lz4 comparator.  Closes: #901548; !4

[ Paul Wise ]
* Clear the progress bar after completion.  Closes: #901758
  Handle terminals that do not support erasing the line by filling the
  terminal with spaces.  Ignore output devices that are not terminals.
* Do not delete the current terminal line for every progress bar update.
  The erasure was also causing the progress bar to flicker.

[ Mattia Rizzolo ]
* Add a gitlab CI script.  !8
* tempfiles:
  + Set the base directory only if the caller didn't specify one already.
  + Do not append '_diffoscope' to the temporary file names, as they are now
    all in their namespaced directory already.
  + Empty the list of known tempfiles after removing them.  Closes: #902709
* external_tools:
  + Fix package name, s/xmlutils/xmlbeans/.
* tests/test_tools: fix test if /sbin contains a directory.  MR: !2
  Thanks to Chris Lamb <lamby@debian.org> for the patch.
* logging:
  + Move the computation of the terminal line eraser into logging.py
  + Always clean the line before printing a log message
* main:
  + Clean the terminal line before printing a traceback.
  + Be sure to print 'Keyboard Interrupt' if that happens.
* comparators:
  + Do not shadow the original import errors.  MR: !7
* d/control:
  + Bump Standards-Version to 4.1.5, no changes needed.
  + Add Build-Dependency on procyon-decompiler, to run the tests.
* d/tests:
  + Autogenerate d/tests/control with all the recommends listed as
    dependencies, so autopkgtest is appropriately triggered whenever one of
    those packages changes.  Closes: #902920

[ anthraxx ]
* comparators.utils:libarchive:
  + Add compatibility code for python-libarchive >= 2.8.  MR: !6
```

You find out more by [visiting the project homepage](https://diffoscope.org).
