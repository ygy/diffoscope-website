---
layout: post
title: diffoscope 162 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `162`. This version includes the following changes:

```
[ Chris Lamb ]
* Don't depends on radare2 in the Debian autopkgtests as it will not be in
  bullseye due to security considerations (#950372). (Closes: #975313)
* Avoid "Command `s p a c e d o u t` failed" messages when creating an
  artificial CalledProcessError instance in our generic from_operation
  feeder creator.
* Overhaul long and short descriptions.
* Use the operation's full name so that "command failed" messages include
  its arguments.
* Add a missing comma in a comment.

[ Jelmer Vernooĳ ]
* Add missing space to the error message when only one argument is passed to
  diffoscope.

[ Holger Levsen ]
* Update Standards-Version to 4.5.1.

[ Mattia Rizzolo ]
* Split the diffoscope package into a diffoscope-minimal package that
  excludes the larger packages from Recommends. (Closes: #975261)
* Drop support for Python 3.6.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
