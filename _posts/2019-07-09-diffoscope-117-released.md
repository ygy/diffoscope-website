---
layout: post
title: diffoscope 117 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `117`. This version includes the following changes:

```
[ Chris Lamb ]
* Add support for file 5.37. Thanks again to Christoph Biedl for the heads-up
  in advance. (Closes: reproducible-builds/diffoscope/#57)
* Apply patch from Gianfranco Costamagna to fix autopkgtest failures in
  Debian. (Closes: #931709)

[ Holger Levsen ]
* debian/control: Bump Standards-Version to 4.4.0.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
