---
layout: post
title: diffoscope 69 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `69`. This version includes the following changes:

```
[ Chris Lamb ]
* Skip tests if binutils can't handle the object file format. Based on a
  patch by Mattia Rizzolo. (Closes: #851588)
* Move external tool definitions out of misleading "exceptions" module.
* Save some complicated logic by setting default RE_FILE_{EXTENSION,TYPE}
* Test --html-dir option.
* Misc:
  * Add missing `data` imports.
  - Inherit GzipFile from File, not object.
  - Remove unused imports in comparator tests.
  - Consistently space out environment exports in debian/rules.

[ Mattia Rizzolo ]
* If both RE_FILE_TYPE and RE_FILE_EXTENSION are defined, AND them
* Use the path attribute of the specialized file instead of the original name
  in tests.
* tests/main:
  * Shorten argument lists by unpacking common arguments.
  * Disable jQuery while testing --htmldir so tests can run without it.
* tests/comparators:
  * Refactor into packages with smaller modules.
  * Rename load_fixture() to init_fixture().
  * Add a load_fixture() function wrapping both init_fixture() and data().
```

You find out more by [visiting the project homepage](https://diffoscope.org).
