---
layout: post
title: diffoscope 7 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `7`. This version includes the following changes:

```
[ Jérémy Bobbio ]
* Add support for PDF files.
* Add --max-report-size command-line option.
* Add extra mime type for .deb.
* Fallback on binary comparison when external tools fail. (Closes: #764140)
* Update copyrights.

[ Helmut Grohne ]
* Avoid unnecessary guess_mime_type calls.

[ Reiner Herrmann ]
* Speed up creation of diffs by removing extra lines before calling vim.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
