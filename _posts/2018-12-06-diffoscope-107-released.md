---
layout: post
title: diffoscope 107 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `107`. This version includes the following changes:

```
* Compare .zip file comments with zipnote. (Closes: #901757)
* Don't emit a large number of warnings if getfacl(1) is not available. This
  makes the behaviour consistent with if lsattr(1) is unavailable.
  (Closes: #902369)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
