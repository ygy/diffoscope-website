---
layout: post
title: diffoscope 183 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `183`. This version includes the following changes:

```
[ Chris Lamb ]
* Add support for extracting Android signing blocks.
  (Closes: reproducible-builds/diffoscope#246)
* Format debug messages for elf sections using our
  diffoscope.utils.format_class utility.
* Clarify a comment about the HUGE_TOOLS dict in diffoscope.external_tools.

[ Felix C. Stegerman ]
* Clarify output around APK Signing Blocks and remove an accidental duplicate
  "0x" prefix.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
