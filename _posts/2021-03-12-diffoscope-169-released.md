---
layout: post
title: diffoscope 169 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `169`. This version includes the following changes:

```
[ Chris Lamb ]
* Optimisations:
  - Use larger buffer/block sizes when extracting files from libarchive-
    based archives.
  - Use a much-shorter CSS class (instead of "diffponct") to dramatically
    reduce uncompressed HTML output.

* Logging improvements:
  - Don't emit "Unable to stat file" warning/debug messages; we have
    entirely-artificial directory entries such as ELF sections which, of
    course, never exist as filesystem files.
  - Don't emit a "Returning a FooContainer" logging message - we already emit
    "Instantiating a FooContainer" one and are unlikely to fail in the
    middle.
  - Format the report size logging messages when generating HTML reports.
  - Add the target directory when logging which directory we are extracting
    containers to.

* Miscellaneous:
  - Ignore "--debug" and similar arguments when creating a (hopefully useful)
    temporary directory.
  - Ensure all internal temporary directories have useful names.
  - Clarify a comment regarding diffoscope not extracting excluded files.

[ Vagrant Cascadian ]
* Skip a DEX-related test if the "procyon" tool is unavailable.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
