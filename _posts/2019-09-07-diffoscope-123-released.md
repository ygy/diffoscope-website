---
layout: post
title: diffoscope 123 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `123`. This version includes the following changes:

```
[ Chris Lamb ]
* Build OCaml test input files on-demand rather than shipping them with the
  package in order to prevent test failures with OCaml 4.08.
  (Closes: #939386)
* Update test skipping messages:
  - When skipping tests due to the lack of installed tool, print the package
    that may provide it.
  - Update "requires foo module" messages to clarify that they are regarding
    Python modules, not packages.
* Rebuild the test squashfs images to exclude the character device as they
  requires root or fakeroot to extract. (reproducible-builds/diffoscope#65)
* Remove accidentally-committed test fixture generation code from tests.
* Set long_description_content_type to 'text/x-rst' in setup.py to appease
  the PyPI.org linter.

[ Mattia Rizzolo ]
* Don't crash when the progress is requested but the Python module is
  missing. (Closes: #939085)

[ Vagrant Cascadian ]
* Add external tools for gifbuild, javap and kbxutil in GNU Guix.

[ Marc Herbert ]
* In the cbfs tests, add a "Comp" column test to support Coreboot
  utils > 4.6.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
