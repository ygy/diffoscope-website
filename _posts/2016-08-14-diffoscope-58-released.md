---
layout: post
title: diffoscope 58 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `58`. This version includes the following changes:

```
* Shorten Description of trydiffoscope to appease Lintian.
* Make squashfs test_listing test more robust:
  - Add ".in" suffix to squashfs listing expected diff as we are about to
    template it properly.
  - Replace $USER with the current user with uid 1000.
  - Move test_listing replacements to a loop so we can easily add more.
  - Don't assume that the test environment as 4 CPUs in squashfs.test_listing
  - Don't assume default group is the same name as the user in test_listing.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
