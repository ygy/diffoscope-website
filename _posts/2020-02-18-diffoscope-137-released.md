---
layout: post
title: diffoscope 137 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `137`. This version includes the following changes:

```
* Also extract classes2.dex, classes3.dex etc. from .apk files.
  (Closes: reproducible-builds/diffoscope#88)
* Add generalised support for "ignoring" returncodes and move special-casing
  of returncodes in the zip comparator to this.
* Accommodate sng returning with a UNIX exit code of 1 even if there minor
  errors in the file (discovered via #950806).
```

You find out more by [visiting the project homepage](https://diffoscope.org).
