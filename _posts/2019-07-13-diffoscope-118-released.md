---
layout: post
title: diffoscope 118 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `118`. This version includes the following changes:

```
* Don't fail in autopkgtests when, for example, we do not have sufficiently
  newer or older version of file. (Closes: #931881)
* Also include python3-tlsh in test dependencies.
* Tidy handling of DIFFOSCOPE_FAIL_TESTS_ON_MISSING_TOOLS:
  - Merge two previously-overlapping environment variables.
  - Use repr(..)-style output when printing test status.
  - Add some explicit return values to appease pylint.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
