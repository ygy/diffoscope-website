---
layout: post
title: diffoscope 47 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `47`. This version includes the following changes:

```
* Don't show a difference for identical but long diff inputs (e.g. objdump
  output). This should make diffoscope run longer for large diff inputs, but
  more correct reports worths it.
* Fix symbol ignore regexp for ELF files.
* Ask readelf to decompress sections before dumping their content.
* Don't redefine .name() in Archive when its already defined in File.
* All files can now belong to a container. This will help us implement
  cross-file analysis.
* Update requirements for ELF tests.
* Sort file order when comparing directories.
* Use Python OrderedDict instead of keeping a list of sections in ELF
  container.
* Install detached symbols from debug .deb before comparing ELF files.
  This means objdump output should have line numbers for Debian packages
  built with recent debhelper as long as the associated debug package is in
  the same directory.
* Add support for Debian .buildinfo files.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
