---
layout: post
title: diffoscope 6 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `6`. This version includes the following changes:

```
* Fallback to binary comparison when text encoding gets misdetected.
* Skip full comparison when small files match.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
