---
layout: post
title: diffoscope 16 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `16`. This version includes the following changes:

```
[ Reiner Herrmann ]
* Remove temporary directory when exception is raised. (Closes: #782253)
* Fix extraction when Zip member contain "../" in their path.

[ Jérémy Bobbio ]
* Fail with a better error message when unable to create a Queue.
  (Closes: #782551)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
