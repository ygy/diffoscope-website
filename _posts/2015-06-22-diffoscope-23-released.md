---
layout: post
title: diffoscope 23 released
author: Reiner Herrmann <reiner@reiner-h.de>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `23`. This version includes the following changes:

```
[ Helmut Grohne ]
* Speed up diff collection.
* Read lines in larger chunks.

[ Reiner Herrmann ]
* Don't follow symlinks, but print their targets.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
