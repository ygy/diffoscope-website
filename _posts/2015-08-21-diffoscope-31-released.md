---
layout: post
title: diffoscope 31 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `31`. This version includes the following changes:

```
[ Reiner Herrman ]
* Fix detection of jar archives.
* Make some hardcoded limits configurable. (Closes: #795292)

[ Mattia Rizzolo ]
* Don't print the traceback when killed by KeyboardInterrupt.
* Add a --debugger option to dump the user into pdb in case of crashes.
  (Closes: #796204)

[ Jérémy Bobbio ]
* Replace ssdeep by tlsh

[ Chris Lamb ]
* Unify to 4 spaces everywhere.
* Leave string interpolation to logging.$level(..) calls.
* Add missing 'sys' import.
* Specify source of 'Files' correct; was using "Changes" leaked from for loop.
* Remove unused and duplicate imports.
* Support passing directories as "foo/" or "foo".
```

You find out more by [visiting the project homepage](https://diffoscope.org).
