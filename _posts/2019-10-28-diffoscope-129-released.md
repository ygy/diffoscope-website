---
layout: post
title: diffoscope 129 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `129`. This version includes the following changes:

```
* Call R's "deparse" function to ensure that we do not error out and revert
  to a binary diff when processing .rdb files with internal "vector" types as
  they do not automatically coerce to strings.
* Add the ability to pass Python bytestrings to external commands and
  pass our long script to parse R .rdb files using this new method over a
  long command-line argument
* Use Rscript's --vanilla option over --no-environ as this also enables
  --no-save, --no-restore, --no-site-file and --no-init-file.
* Improve command-line error messages:
  - Split out formatting into a separate utility function.
  - Truncate very long lines when displaying them as an external source
    of data.
  - When printing an error from a command, format the command for the user.
* Use "exit code" over "return code" when referring to UNIX error codes in
  displayed differences.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
