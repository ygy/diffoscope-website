---
layout: post
title: diffoscope 22 released
author: Reiner Herrmann <reiner@reiner-h.de>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `22`. This version includes the following changes:

```
* Add myself to uploaders
* Swap JDK dependencies, so openjdk is preferred (thanks mapreri)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
