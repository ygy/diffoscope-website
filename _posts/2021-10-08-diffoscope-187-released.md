---
layout: post
title: diffoscope 187 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `187`. This version includes the following changes:

```
* Add support for comparing .pyc files. Thanks to Sergei Trofimovich.
  (Closes: reproducible-builds/diffoscope#278)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
