---
layout: post
title: diffoscope 139 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `139`. This version includes the following changes:

```
[ Mattia Rizzolo ]
* Fix Build-Depends on python3-pdfminer. (Closes: #955645)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
