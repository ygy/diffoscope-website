---
layout: post
title: diffoscope 56 released
author: Reiner Herrmann <reiner@reiner-h.de>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `56`. This version includes the following changes:

```
[ Jérémy Bobbio ]
* Fix maybe_decode() so that it actually works.

[ Chris Lamb ]
* Mask EPIPE to avoid ugly tracebacks when eg. prematurely terminating $PAGER.
* Add git index file comparator.
* Drop unused imports in Ar comparator.

[ Ximin Luo ]
* Add support for AR archives (including Rust .rlib files).
* Add some more documentation for ArFile/ArContainer.
* RequiredToolNotFound.PROVIDERS dict values are package names, not binary
  names.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
