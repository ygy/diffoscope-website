---
layout: post
title: diffoscope 37 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `37`. This version includes the following changes:

```
* Switch to incremental interface for TLSH. That means we won't load
  800 MiB or more in memory to compute the fuzzy hash.
* Add support for CBFS images (Closes: #788364)
* Add support for .dsc files (Closes: #800359)
* Fix handling of malformed md5sums file. (Closes: #799901)
* Fix placeholder encoding issue when trimming stderr.
  (Closes: #799863)
* Read text report as UTF-8 encoded in test_text_option_with_file.
  (Closes: #801766)
* Read Haskell interfaces in binary mode (Closes: #801333)
* Read md5sums file using UTF-8.
* Add a test for --max-diff-block-lines.
* Handle closing stdin for Command implementing feed_stdin.
* Handle subprocess errors with no output when comparing with a command.
* Use name and not path when doing binary comparisons.
* Add a test for --list-tools.
* Fix main exception handling.
* Use text file comparison for .dsc and .changes when the referenced files
  are missing.
* Do skip squashfs test if there's no user with uid 1000.
* Use super() instead of the old cumbersome form.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
