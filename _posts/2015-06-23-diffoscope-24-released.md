---
layout: post
title: diffoscope 24 released
author: Reiner Herrmann <reiner@reiner-h.de>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `24`. This version includes the following changes:

```
* Fix for undefined variables introduced in last release.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
