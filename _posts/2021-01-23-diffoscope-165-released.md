---
layout: post
title: diffoscope 165 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `165`. This version includes the following changes:

```
[ Dimitrios Apostolou ]
* Introduce the --no-acl and --no-xattr arguments [later collapsed to
  --extended-filesystem-attributes] to improve performance.
* Avoid calling the external stat command.

[ Chris Lamb ]
* Collapse --acl and --xattr into --extended-filesystem-attributes to cover
  all of these extended attributes, defaulting the new option to false (ie.
  to not check these very expensive external calls).

[ Mattia Rizzolo ]
* Override several lintian warnings regarding prebuilt binaries in the
* source.
* Add a pytest.ini file to explicitly use Junit's xunit2 format.
* Ignore the Python DeprecationWarning message regarding the `imp` module
  deprecation as it comes from a third-party library.
* debian/rules: filter the content of the d/*.substvars files
```

You find out more by [visiting the project homepage](https://diffoscope.org).
