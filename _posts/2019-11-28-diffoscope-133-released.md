---
layout: post
title: diffoscope 133 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `133`. This version includes the following changes:

```
* Correct the substitution/filtering of paths in ELF output to avoid
  unnecessary differences depending on the path name provided on the
  commandline. (Closes: #945572)
* Silence/correct a SyntaxWarning message due to incorrectly comparing an
  integer by identity (is) over equality (==). (Closes: #945531)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
