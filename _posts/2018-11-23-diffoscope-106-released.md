---
layout: post
title: diffoscope 106 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `106`. This version includes the following changes:

```
[ Chris Lamb ]
* comparators:
  + elf: Don't assume all files called ".a" are ELF binaries.  Closes: #903446
  + pdf:
    - Display the reason when cannot extract metadata from PDF files.
    - Prevent tracebacks when obtaining PDF metadata from files with multiple
      PDF dictionary definition entries.  Closes: #913315

[ Marius Gedminas ]
* Add a python_requires to setup.py.

[ Mattia Rizzolo ]
* debian:
  + Disable depends on gnumeric and procyon as they are RC buggy.
  + Override the new lintian's public-upstream-key-in-native-package.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
