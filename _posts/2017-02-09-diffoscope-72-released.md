---
layout: post
title: diffoscope 72 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `72`. This version includes the following changes:

```
* Fix autopkgtest failures when Recommends are not installed.
  (Closes: #854593)
* Specify <html lang="en"> in HTML output. (re. #849411)
* Tests:
  - Add a "@skip_unless_module_exists" decorator.
  - Show local variables in tracebacks.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
