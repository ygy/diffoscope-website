---
layout: post
title: diffoscope 78 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `78`. This version includes the following changes:

```
[ Brett Smith ]
* comparators.json:
  + Catch bad JSON errors on Python pre-3.5.  Closes: #855233

[ Chris Lamb ]
* tests:
  + Move normalize_zeros to more generic `utils.data` module.
  + Fix tests that call xxd fail on jessie due to output change.
    Closes: #855239

[ Ed Maste ]
* comparators.directory:
  + Assume BSD-style stat(1) on FreeBSD.  Closes: #855169
```

You find out more by [visiting the project homepage](https://diffoscope.org).
