---
layout: post
title: diffoscope 12 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `12`. This version includes the following changes:

```
[ Helmut Grohne ]
* Attempt at fixing unicode issues with --text. (Closes: #778641)

[ Jérémy Bobbio ]
* Try with utf-8 encoding when comparing text files and no common
  encoding has been detected.
* Perform content comparison when finding differences instead of
  waiting for the presentation phases. (Closes: #781280)
* Instead of using vim, the HTML view is created from diff output.
  (Closes: #772029, #779476)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
