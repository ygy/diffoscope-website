---
layout: post
title: diffoscope 48 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `48`. This version includes the following changes:

```
* Open debian/control file in binary mode (Closes: #812524)
* Fix recognizes() for files ending in .buildinfo but without a
  Checksums-Sha256 field. (Closes: #812534)
* Fix finding debug packages with multiple Build-Ids.
* Cleanup how arguments are given to readelf for sections.
* Only pass --decompress to readelf when the option is supported. This
  restores compatibility with older versions of binutils.
* Skip dbgsym tests if the debian Python module is not available.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
