---
layout: post
title: diffoscope 103 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `103`. This version includes the following changes:

```
[ Mattia Rizzolo ]
* In Debian, list liblz4-tool as an alternative to the lz4 package.
  stretch only has the former.

[ Chris Lamb ]
* Strip trailing whitespace from ssconvert(1) output to support
  gnumeric 1.12.43.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
