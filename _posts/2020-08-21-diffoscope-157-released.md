---
layout: post
title: diffoscope 157 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `157`. This version includes the following changes:

```
[ Chris Lamb ]

* Try obsensibly "data" files named .pgp against pgpdump to determine whether
  they are PGP files. (Closes: reproducible-builds/diffoscope#211)
* Don't raise an exception when we encounter XML files with "<!ENTITY>"
  declarations inside the DTD, or when a DTD or entity references an external
  resource. (Closes: reproducible-builds/diffoscope#212)
* Temporarily drop gnumeric from Build-Depends as it has been removed from
  testing due to Python 2.x deprecation. (Closes: #968742)
* Codebase changes:
  - Add support for multiple file extension matching; we previously supported
    only a single extension to match.
  - Move generation of debian/tests/control.tmp to an external script.
  - Move to our assert_diff helper entirely in the PGP tests.
  - Drop some unnecessary control flow, unnecessary dictionary comprehensions
    and some unused imports found via pylint.
* Include the filename in the "... not identified by any comparator"
  logging message.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
