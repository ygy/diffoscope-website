---
layout: post
title: diffoscope 17 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `17`. This version includes the following changes:

```
[ Jérémy Bobbio ]
* Add support for ISO9660 images.
* Catch a few more initrds as such.
* Reimplement are_same_binaries() using cmp.

[ Reiner Herrmann ]
* Use gzip comparator for .dz (dictzip) files.
* Added isoinfo to list of tool providers.

[ Yasushi SHOJI ]
* Disable 'Device' section diff in stat output. (Closes: #783792)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
