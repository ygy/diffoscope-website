---
layout: post
title: diffoscope 51 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `51`. This version includes the following changes:

```
* Team upload.
* Re-upload, the last uploaded tarball was broken, missing test files.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
