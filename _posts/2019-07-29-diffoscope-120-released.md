---
layout: post
title: diffoscope 120 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `120`. This version includes the following changes:

```
* No-change sourceful after accidentally uploading binaries in order to
  migration to testing.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
